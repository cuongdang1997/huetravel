<!DOCTYPE html>
	<html>
	<head>
		<title>Hue Traveling</title>
		<!-- header -->
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<meta name="viewport" content="width: device-width, initial-scale =1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style type="text/css"></style>
		<!-- end header -->
	</head>
	<body>
	<div id="wrapper">
		<div class="header">
			<!-- strat-contact -->
				<div class="row hidden-sm hidden-xs" id="heading">					
					<div class="container">
						<div class="col-md-6 info-contact">
							<i class="fa fa-phone"></i><span> 024 7109 9999</span>
							<i class="fa fa-envelope"></i><span> dulichhue@gmail.com</span>
						</div>
					</div>					
				</div>
			<!-- end-contact -->
			<!-- start menu -->
			<nav class="navbar navbar-expand-lg navbar-light main-menu">
				<div class="container">
				<a class="navbar-brand" href="index.php"><img src="image/logo.png" height="45px" width="45px"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php" title="Trang chủ">Trang chủ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#addressTour" title="Địa điểm du lịch">Địa điểm du lịch</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#foodTour" title="Ẩm thực Huế">Ẩm thực Huế</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="traveltour.php" title="Tour du lịch">Tour</a>
						</li>
						<li class="nav-item">
							<a class="nav-link btn btn-primary text-white text-uppercase ml3" href="#">Đăng nhập</a>
						</li>
						<li>

							
						</li>

					<!-- <li><a href="#" title="Tour">Tour</a></li>
						<li><a href="#" title="Đăng nhập">Đăng nhập</a></li> -->
					</ul>
				</div>
			</div>
	</div>
</div>
</nav>
			<!-- end menu -->
			<!-- start banner -->
			<div class="bg-banner">
				<div class="container wp-banner">
					<div class="row">
						<div class="col-sm-12 col-md-4 col-lg-4">
							<div class="news">
								<ul class="list-group">
									<li class="list-group-item list-main-title"><h6>Địa điểm du lịch nổi bật</h6></li>
									<li class="list-group-item">First item</li>
									<li class="list-group-item">Second item</li>
									<li class="list-group-item">Third item</li>
									<li class="list-group-item">Second item</li>
									<li class="list-group-item">Third item</li>
								</ul>
							</div>
						</div>
						<div class="col-sm-12 col-md-8 col-lg-8 hidden-sm">
							<div id="banner" class="container w-100 h-50 p-0">
								<div class="bd-example">
									<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
										<ol class="carousel-indicators">
											<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
											<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
											<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
										</ol>
										<div class="carousel-inner">
											<div class="carousel-item active">
												<center><img src="image/hue1.jpg" class="d-block banner-img" alt="..."></center>
												<div class="carousel-caption d-none d-md-block">
													<h5>Cầu Trường Tiền</h5>
													<p>Cầu Trương Tiền sáu vài mười hai nhịp <br>
														Anh đi không kịp trách ai không chờ
													</p>
												</div>
											</div>
											<div class="carousel-item">
												<center><img src="image/hue2.jpg" class="d-block banner-img" alt="..."></center>
												<div class="carousel-caption d-none d-md-block">
													<h5>Đại Nội Huế</h5>
													<p>Quần thể di tích Cố đô được UNESCO công nhận là Di sản Văn hóa Thế giới</p>
												</div>
											</div>
											<div class="carousel-item">
												<center><img src="image/hue3.jpg" class="d-block banner-img" alt="..."></center>
												<div class="carousel-caption d-none d-md-block">
													<h5>Cổng Ngọ Môn</h5>
													<p>Cổng dẫn vào Kinh thành Huế.</p>
												</div>
											</div>
										</div>
										<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
		</div>