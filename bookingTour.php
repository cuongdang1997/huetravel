<?php
require_once "common/connect.php";
require_once "./common/sysenv.php";
//get post in by ID
if (isset($_GET["id"])) {
	$id = $_GET["id"];
	$stmt = $conn -> prepare($getSQL["getTourById"]);
	$stmt -> bind_param("i", $id);
	$stmt -> execute();
	$stmt -> bind_result($result_id, $result_name, $result_description, $result_count , $result_files, $id_tour_type, $result_date_start, $result_status, $result_price, $result_place_start, $result_translation);
	$stmt -> fetch();
	$stmt -> close();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
	if(isset($_POST["submit"])){
		$mess = "";
		$id_tour = $_GET["id"];
		$fullname = $_POST["fullname"];
		$gender = $_POST["gender"];
		$birthday = $_POST["birthday"];
		$address = $_POST["address"];
		$phoneNumber = $_POST["phoneNumber"];
		$gmail = $_POST["gmail"];
		$date_start = $result_date_start;
		$numberOfPeople = $_POST["numberOfPeople"]; 
		$numberOfChild = $_POST["numberOfChild"];
		if($fullname == "" || $gender == "" || $birthday == "" || $address == "" || $gmail == ""){
			$mess = "Vui lòng nhập đầy đủ thông tin";
		} else  if($mess == ""){
    	// tạo mới customer
			$stmt = $conn -> prepare($getSQL["iCustomer"]);
			$stmt -> bind_param("ssssss", $fullname, $gender, $birthday, $address, $phoneNumber, $gmail);
			$stmt -> execute();
			$stmt -> close();
      // get new customer 
			$newCustomer = $conn->query($getSQL["getNewCustomer"])->fetch_assoc()["newId"];

			$stmt = $conn -> prepare($getSQL["iBookingTour"]); 
			$stmt -> bind_param("ssiii",  $newCustomer, $id, $numberOfPeople, $numberOfChild, $result_price);
			if($stmt -> execute()){
				$mess = "Đặt tour thành công! 
				Cám ơn quý khách đã tin dùng dịch vụ du lịch của chúng tôi!	";
			}else {
				$mess = "Đặt tour thất bại";
			}
			$stmt -> close();
     }       //
 }
}
?>
<!DOCTYPE html>
<html>
<head>
	<title>Hue Traveling</title>
	<!-- header -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" type="text/css" href="css/tempusdominus-bootstrap-4.min.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- end header -->
</head>
<body>
	<div id="wrapper">
		<div class="header">
			<!-- strat-contact -->
			<div class="row hidden-sm hidden-xs" id="heading">					
				<div class="container">
					<div class="col-md-6 info-contact">
						<i class="fa fa-phone"></i><span> 024 7109 9999</span>
						<i class="fa fa-envelope"></i><span> dulichhue@gmail.com</span>
					</div>
				</div>					
			</div>
			<!-- end-contact -->
			<!-- start menu -->
			<nav class="navbar navbar-expand-lg navbar-light main-menu">
				<div class="container">
					<a class="navbar-brand" href="index.php"><img src="image/logo.png" height="45px" width="45px"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>

					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="index.php" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#addressTour" title="Địa điểm du lịch">Địa điểm du lịch</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#foodTour" title="Ẩm thực Huế">Ẩm thực Huế</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="traveltour.php" title="Tour du lịch">Tour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link btn btn-primary text-white text-uppercase ml3" href="#">Đăng nhập</a>
							</li>
							<li>
							</li>

					<!-- <li><a href="#" title="Tour">Tour</a></li>
						<li><a href="#" title="Đăng nhập">Đăng nhập</a></li> -->
					</ul>
				</div>
			</div>
		</nav>
		</div>
	</div>
<div class="main">
	<div class="container wp-banner">
		<div class="row">
			<div class="col-sm-12 col-md-4 col-lg-4">
				<div class="tour border p-3 shadow">
					<form method="POST">
						<input id="tourId" hidden value="<?php echo $result_id; ?>">
						<div style="border: 1px;"> <h4> 1. Thông tin liên lạc</h4>
							<div class="form-group">
								<label for="exampleInputEmail1">Họ tên</label>
								<input type="name" class="form-control" id="exampleInputName" placeholder="Nhập họ và tên" name="fullname">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Giới tính</label>
								<select class="form-control" name="gender" required>
      									<option value="0">Nam</option>
       									<option value="1">Nữ</option>
      									<option value="2">Khác</option>
      								</select>
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Ngày sinh</label>
								<input name="birthday" type="text" class="form-control" id="exampleInputBirthday" placeholder="Nhập ngày sinh">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Địa chỉ</label>
								<input name="address" type="address" class="form-control" id="exampleInputAddress" placeholder="Nhập địa chỉ">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Số điện thoại</label>
								<input name="phoneNumber" type="phone" class="form-control" id="exampleInputPhone" placeholder="Nhập số điện thoại">
							</div>		
							<div class="form-group">
								<label for="exampleInputPassword1">Gmail</label>
								<input name="gmail" type="gmail" class="form-control" id="exampleInputGmail" aria-describedby="emailHelp" placeholder="Nhập gmail">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Ngày khởi hành</label>
								<input disabled="true" type="text" class="form-control" id="date_start" name="date_start" value="<?php echo $result_date_start ?>" >
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Số người</label>
								<input name="numberOfPeople" type="number" class="form-control" id="numberOfPeople" placeholder="Nhập số người" onchange="handlePrice()">
							</div>
							<div class="form-group">
								<label for="exampleInputPassword1">Số trẻ em</label>
								<input name="numberOfChild" type="number" class="form-control" id="numberOfChild" placeholder="Nhập số trẻ em" onchange="handlePrice()">
							</div>
						</div>

						<div class="form-group text-center">
			              <button type="submit" name="submit" value="submit" class="btn btn-primary btn-block">Hoàn thành</button>
			            </div>
					</form>
				</div>
			</div>
			<hr>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<table class="table table-striped  border p-3 shadow">
					<tr>
						<th scope="col"><h4 class="mb-0">2. Thông tin đặt tour</h4></th>
					</tr>
					<tr>
						<th scope="row"><img style="width: 100% ;height: auto;" src="image/DaiNoi1.jpg"></th>
					</tr>
					<tr>
						<th scope="row"><p><?php echo $result_name ?></p></th>     
					</tr>
					
					
				</table>
				<table class="border p-3 shadow" style="background-color: #b3e6ff;">
					<tr>
						<th id="submit_booking" style="width:350px" scope="col" class="p-3 mb-2 text-white btn btn-primary btn-block"><h5 class="mb-0">Thông tin thanh toán</h5></th>
					</tr>
					<tr>
						<th scope="row" class="p-2"><span id="adult">0</span> người lớn x <?php echo $result_price; ?></th>
					</tr>
					<tr>
						<th scope="row" class="p-2"><span id="children">0</span> trẻ em x <?php echo $result_price*0.6; ?> đồng</th>
					</tr>
					<tr>
						<th scope="row" class="p-2">Tổng số tiền: <span id="total">0</span> đồng</th>
					</tr>
				</table>
			</div>
			<div class="col-sm-12 col-md-4 col-lg-4">
				<table class="table table-striped border p-3 shadow">
					<tr>
						<th scope="col"><h4 class="mb-0">3. Gửi đơn đặt tour</h4></th>
					</tr>
					<tr>
						<th scope="row">Bạn muốn đặt tour khác?</th>
					</tr>
					<tr>
						<th scope="row">
							Lưu ý:
							<ul class="pl-3">
								<li>Quý khách có thể thanh toán trước bằng thẻ tại đây.</li>
								<li>Mytour sẽ liên hệ với quý khách (qua email hoặc điện thoại) trong vòng 30 phút (T2-CN: 08:00 - 23:00) để xác nhận tour và thời hạn thanh toán.</li>
								<li>Quý khách sẽ thanh toán (tại nhà, tại Mytour, chuyển khoản hay thẻ) sau khi có xác nhận còn tour từ Mytour.</li>
								<li>Trường hợp Quý khách muốn xác nhận ngay, vui lòng liên hệ với Mytour theo Hotline: 
								</li>
							</ul>
						</th>
					</tr>
				</table>
			</div>
		</div>
	</div>
</div>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tempusdominus-bootstrap-4.min.js"></script>
<script>
    function handlePrice() {
		var $numberOfPeople = $("#numberOfPeople").val() || 0;
		var $numberOfChild = $("#numberOfChild").val() || 0;
		if($numberOfPeople < 0 || $numberOfChild < 0){
			alert("Số người phải lớn hơn hoặc bằng 0!");
			$numberOfPeople = $numberOfPeople < 0 ? 0: $numberOfPeople;
			$numberOfChild = $numberOfChild < 0 ? 0: $numberOfChild;			
			$("#numberOfPeople").val($numberOfPeople);
			$("#numberOfChild").val($numberOfChild);
		}else if($numberOfChild > $numberOfPeople/2){
			alert("Số trẻ em không được quá một nữa tổng số người!");
			$numberOfChild=$numberOfChild - 1;
			$("#numberOfChild").val($numberOfChild);
		} else {
			var $id_tour = $("#tourId").val();
			document.getElementById("adult").innerHTML= $numberOfPeople - $numberOfChild;
			document.getElementById("children").innerHTML = $numberOfChild;
			if (window.XMLHttpRequest) {
				// code for IE7+, Firefox, Chrome, Opera, Safari
				xmlhttp=new XMLHttpRequest();
			} else { // code for IE6, IE5
				xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
			}
			xmlhttp.onreadystatechange=function() {
				if (this.readyState==4 && this.status==200) {
					document.getElementById("total").innerHTML=this.responseText;
				}
			}
			xmlhttp.open("GET","handleBookingTour.php?id="+$id_tour+"&pp="+$numberOfPeople+"&ch="+$numberOfChild,true);
			xmlhttp.send();
		}
    }
</script>
</body>
</html>


