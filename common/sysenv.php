<?php
	// SQL query 
	// Admin
	$getSQL["gAdmin"] = "SELECT id, username, password, role FROM admin WHERE username = ?";
	// Thông tin
	$getSQL["getAllMenu"] = "SELECT * FROM menu";
	$getSQL["getMenuById"] = "SELECT id, name, sort_order, status, description FROM menu WHERE id = ?";
	$getSQL["iMenu"] = "INSERT INTO menu(name, sort_order, status, description) VALUES (?, ?, ?, ?)";
	$getSQL["editMenu"] = "UPDATE menu SET name = ?, sort_order = ?, status = ?, description = ? WHERE id = ?";
	$getSQL["deleteMenu"] = "DELETE FROM menu WHERE id = ?";
	// Địa điểm du lịch
	$getSQL["getAllNews"] = "SELECT * FROM news";
	$getSQL["getNewsById"] = "SELECT id, title, description, files,  short_des, sort_order FROM news WHERE id = ?";
	$getSQL["iNews"]= "INSERT INTO news(title, description, files, short_des, sort_order) VALUES (?, ?, ?, ?, ?)";
	$getSQL["editNews"] = "UPDATE news SET title = ?, description = ?, files = ?, short_des = ?, sort_order = ?  WHERE id = ?";
	$getSQL["deleteNews"] = "DELETE FROM news WHERE id = ?";
	// Ẩm thực Huế
	$getSQL["getAllProducts"] = "SELECT * FROM products";
	$getSQL["getProductsById"] = "SELECT id, title, files, short_des, description FROM products WHERE id = ?";
	$getSQL["iProducts"] = "INSERT INTO products(title,  files, short_des, description, menu_id) VALUES (?, ?, ?, ?, ?)";
	$getSQL["editProducts"] = "UPDATE products SET title = ?, files = ?, short_des = ? , description = ? WHERE id = ?";
	$getSQL["deleteProducts"] = "DELETE FROM products WHERE id = ?";
	//
	//Dịch vụ
	$getSQL["getAllService"] = "SELECT * FROM service";
	$getSQL["getServiceById"] = "SELECT id, name, description,  price, files FROM service WHERE id = ?";
	$getSQL["iService"] = "INSERT INTO service(name, description, price,  files) VALUES (?, ?, ?, ?)"; 
	$getSQL["editService"] = "UPDATE service SET name = ?,  description = ?, files = ?, price = ? WHERE id = ?";
	$getSQL["deleteService"] = "DELETE FROM service WHERE id = ?";
	// Phương tiện
	$getSQL["getAllTranslation"] = "SELECT * FROM translation";
	$getSQL["getTranslationById"] = "SELECT id, name, description,  price FROM translation WHERE id = ?";
	$getSQL["iTranslation"] = "INSERT INTO translation(name, description, price) VALUES (?, ?, ?)";
	$getSQL["editTranslation"] = "UPDATE translation SET name = ?,  description = ?, price = ? WHERE id = ?";
	$getSQL["deleteTranslation"] = "DELETE FROM translation WHERE id = ?";
	//Đặt tour
	// $getSQL["getAllBookingtour"] = "SELECT * FROM bookingtour";
	// $getSQL["getBookingtourById"] = "SELECT id, name, description,  price FROM bookingtour WHERE id = ?";
	// $getSQL["iBookingtour"] = "INSERT INTO bookingtour(name,  description, price, bookingtour_id) VALUES (?, ?, ?, ?, ?)";
	// $getSQL["editBookingtour"] = "UPDATE bookingtour SET name = ?,  description = ?, price = ? WHERE id = ?";
	// $getSQL["deleteBookingtour"] = "DELETE FROM bookingtour WHERE id = ?";
	//Khách sạn
	$getSQL["getAllHotel"] = "SELECT * FROM hotel";
	$getSQL["getHotelById"] = "SELECT id, name, phone,  address FROM hotel WHERE id = ?";
	$getSQL["iHotel"] = "INSERT INTO hotel(name,  phone, address) VALUES (?, ?, ?)";
	$getSQL["editHotel"] = "UPDATE hotel SET name = ?,  phone = ?, address = ? WHERE id = ?";
	$getSQL["deleteHotel"] = "DELETE FROM hotel WHERE id = ?";
	$getSQL["iMenuHotel"] = "INSERT INTO menu_hotel(name, phone, address) VALUES(?, ?, ?)";
	$getSQL["gMenuHotel"] = "SELECT id, name FROM menu_hotel WHERE status = ?";

	// Khách hàng
	$getSQL["getAllCustomer"] = "SELECT * FROM customer";
	$getSQL["getCustomerById"] = "SELECT id, name, gender, birthday, address, phone, gmail FROM customer WHERE id = ?";
	$getSQL["iCustomer"] = "INSERT INTO customer(name, gender, birthday, address, phone, gmail) VALUES (?, ?, ?, ?, ?, ?)";
	$getSQL["getNewCustomer"] = "SELECT MAX(id) as newId FROM customer";
	$getSQL["editCustomer"] = "UPDATE customer SET name = ?,  gender = ?, birthday = ?, address = ?, phone = ?, gmail = ? WHERE id = ?";
	$getSQL["deleteCustomer"] = "DELETE FROM customer WHERE id = ?";
	$getSQL["selectMaxIdCustomer"] = "SELECT MAX(id) as maxIdCustomer FROM customer";

	//Dịch Vụ
	$getSQL["iMenuService"] = "INSERT INTO menu_service(name, status) VALUES(?, ?)";
	$getSQL["gMenuService"] = "SELECT id, name FROM menu_service WHERE sub_menu IS NULL ORDER BY id ASC";
	$getSQL["getMenuServiceById"] = "SELECT id, name, sub_menu, short_des, price, files, status FROM menu_service WHERE sub_menu = ?";
	$getSQL["iSubMenuService"] = "INSERT INTO menu_service(sub_menu, name) VALUES(?, ?)";
	// $getSQL["iSubService"] = "INSERT INTO menu_service(sub_menu, name) VALUES(?, ?)";
	$getSQL["iSubService"] = "INSERT INTO service_details(menu_service_id, menu_service_sub_id, price, files, description) VALUES (?, ?, ?, ?)";
	$getSQL["gMenuSelectService"] = "SELECT ms1.id ms1Id, ms1.name ms1Name, ms2.id ms2Id, ms2.name ms2Name FROM menu_service ms1, menu_service ms2 WHERE ms1.id = ms2.sub_menu AND ms2.sub_menu =:id";
	//End Dịch Vụ
	//Phương tiện
	$getSQL["iMenuTranslation"] = "INSERT INTO menu_translation(name, status) VALUES(?, ?)";
	$getSQL["gMenuTranslation"] = "SELECT id, name FROM menu_translation WHERE status = ?";
	$getSQL["getMenuTranslationById"] = "SELECT id, name, sub_menu, sort_order, status FROM menu_translation WHERE sub_menu = ?";
	$getSQL["iSubMenuTranslation"] = "INSERT INTO menu_translation(sub_menu, name) VALUEs(?, ?)";
	//End phương tiện
	//Tour
	$getSQL["getAllTour"] = "SELECT * FROM tour";
	$getSQL["getTourById"] = "SELECT id, name, description, count, files, id_tour_type, date_start, status, price, place_start, translation FROM tour WHERE id = ?";
	// $getSQL["iBookingTour"] = "INSERT INTO bookingtour(name, phone, gmail, address, count_of_people, count_of_child, note, date_booking, price) VALUES (?, ?, ?, ?, ?, ?, ?, ?, ?)";
	$getSQL["iBookingTour"] = "INSERT INTO tour_details( id_Customer, id_Tour, count_of_people, count_of_child, price) VALUES (?, ?, ?, ?, ?)";
	$getSQL["getAllBookedTour"] = "SELECT tour_details.*, customer.name as customerName, customer.phone, tour.name as tourName, tour.date_start, tour.place_start
	 FROM tour_details INNER JOIN customer ON tour_details.id_Customer = customer.id
	INNER JOIN tour ON tour_details.id_Tour = tour.id";
	$getSQL["getBookingTourById"] = "SELECT id, id_customer, id_tour, count_of_people, count_of_child, price FROM  tour_details WHERE id = ?";
	$getSQL["editBookingTour"] = "UPDATE tour_details SET count_of_people = ?, 	 = ?, price = ? WHERE id = ?";
	//End Tour
?>
