<?php
	require_once "common/connect.php";
    require_once "./common/sysenv.php";
    $total = 0;
    if(($_GET["pp"] !=Null) && ($_GET["ch"] != Null) && ($_GET["id"]) != Null){
        $id = $_GET["id"];
        $numberOfPeople = $_GET["pp"];
        $numberOfChild = $_GET["ch"];
        $numberOfAdult = $numberOfPeople - $numberOfChild;
        $stmt = $conn -> prepare($getSQL["getTourById"]);
        $stmt -> bind_param("i", $id);
        $stmt -> execute();
        $stmt -> bind_result($result_id, $result_name, $result_description, $result_count , $result_files, $id_tour_type, $result_date_start, $result_status, $result_price, $result_place_start, $result_translation);
        $stmt -> fetch();
        $stmt -> close();
        $total = $numberOfAdult*$result_price + $numberOfChild*$result_price*0.6;
    }
    echo $total;

?>