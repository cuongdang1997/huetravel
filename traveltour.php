<?php 
require_once "common/connect.php";
require_once "./common/sysenv.php";
?>
<?php

/*get tour list to show*/
$result = $conn->query($getSQL["getAllTour"]);
/*search*/
if ($_SERVER["REQUEST_METHOD"] == "POST") {
  if(isset($_POST["submit"])){
    
    }
  }

?>
<!DOCTYPE html>
<html>
<head>
	<title>Hue Traveling</title>
	<!-- header -->
	<meta charset="utf-8">
	<meta name="viewport" content="width=device-width, initial-scale=1, shrink-to-fit=no">
	<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
	<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
	<link rel="stylesheet" type="text/css" href="css/tempusdominus-bootstrap-4.min.css">
	<link rel="stylesheet" type="text/css" href="css/style.css">
	<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
	<!-- end header -->
</head>
<body>
	<div id="wrapper">
		<div class="header">
			<!-- strat-contact -->
			<div class="row hidden-sm hidden-xs" id="heading">					
				<div class="container">
					<div class="col-md-6 info-contact">
						<i class="fa fa-phone"></i><span> 024 7109 9999</span>
						<i class="fa fa-envelope"></i><span> dulichhue@gmail.com</span>
					</div>
				</div>					
			</div>
			<!-- end-contact -->
			<!-- start menu -->
			<nav class="navbar navbar-expand-lg navbar-light main-menu">
				<div class="container">
					<a class="navbar-brand" href="index.php"><img src="image/logo.png" height="45px" width="45px"></a>
					<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
						<span class="navbar-toggler-icon"></span>
					</button>
					<div class="collapse navbar-collapse" id="navbarSupportedContent">
						<ul class="navbar-nav ml-auto">
							<li class="nav-item">
								<a class="nav-link" href="index.php" title="Trang chủ">Trang chủ</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#addressTour" title="Địa điểm du lịch">Địa điểm du lịch</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="#foodTour" title="Ẩm thực Huế">Ẩm thực Huế</a>
							</li>
							<li class="nav-item">
								<a class="nav-link" href="traveltour.php" title="Tour du lịch">Tour</a>
							</li>
							<li class="nav-item">
								<a class="nav-link btn btn-primary text-white text-uppercase ml3" href="#">Đăng nhập</a>
							</li>
							<li>
							</li>
					<!-- <li><a href="#" title="Tour">Tour</a></li>
						<li><a href="#" title="Đăng nhập">Đăng nhập</a></li> -->
					</ul>
				</div>
			</div>
		</div>
	</div>
</div>
<hr>
<div class="main pb-5">
	<div class="container wp-banner">
		<div class="row">
			<div class="col-sm-12 col-md-3 col-lg-3">
				<br>
				<div class="tour border p-3 shadow rounded">
					<table>
						<tr>
							<form>
								<div class="form-group">
									<label for="formGroupExampleInput"><strong>Bạn muốn chọn loại tour nào?</strong></label>
									<select class="form-control" name="status">
										<option value="0">Tour du lịch</option>
										<option value="1">Tour trải nghiệm</option>
									</select>
								</div>
								<div class="form-group">
									<label for="formGroupExampleInput2">Thời gian khởi hành</label>								
									<input type="text" class="form-control" placeholder="Chọn ngày" id="datetimepickerTour_in" data-toggle="datetimepicker" data-target="#datetimepickerTour_in">
								</div>
								<div class="form-group">
									<label for="formGroupExampleInput2">Thời gian kết thúc</label>								
									<input type="text" class="form-control" placeholder="Chọn ngày" id="datetimepickerTour_out" data-toggle="datetimepicker" data-target="#datetimepickerTour_out">
								</div>
								<button type="submit" class="btn btn-danger btn-block"> Tìm kiếm </button>
							</form>
						</tr>
					</table>
				</div>
				<hr>
				<div class="tour border p-3 rounded shadow">
					<table>
						<tr>
							<form>
								<div class="form-group ">
									<label for="formGroupExampleInput"><strong>Tour liên quan </strong></label><hr>
								</div>
								<div id="place"> 
									<div class="row place-related ">	
										<div class="col-sm-12 col-md-12 col-lg-12 mb-3">
											<div class="row">
												<div class="col-sm-6"><img src="image/MinhMang.jpg" class="img-fluid tour-related-img rounded-lg"></div>
												<div class="col-sm-6">Tham quan quần thể lăng tẩm Huế </div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</tr>
					</table>
				</div>
				<hr>
				<div class="tour border p-3 rounded shadow">
					<table>
						<tr>
							<form>
								<div class="form-group ">
									<label for="formGroupExampleInput"><strong>Gợi ý ấm thực Huế </strong></label><hr>
								</div>
								<div id="place"> 
									<div class="row place-related ">	
										<div class="col-sm-12 col-md-12 col-lg-12 mb-3">
											<div class="row">
												<div class="col-sm-6"><img src="image/nampho.jpg" class="img-fluid tour-related-img rounded-lg"></div>
												<div class="col-sm-6">Bánh canh Nam Phổ </div>
											</div>
										</div>
									</div>
								</div>
							</form>
						</tr>
					</table>
				</div>
					</div>
					<hr>
					<div class="col-sm-12 col-md-9 col-lg-9 hidden-sm">
						<?php
						 if ($result->num_rows > 0) {
                			while($rs = $result->fetch_assoc()) { ?>
						<div>
							<h5><?php echo $rs["name"]; ?></h5>
							<div class="row place-related ">
								<div class="col-sm-4"><img src="image/DaiNoi2.jpg" class="img-fluid"></div>
								<div class="col-sm-4">
									<p><strong>Ngày khởi hành:</strong><?php echo $rs["date_start"]; ?> <br>
										<strong>Thời gian:</strong> <?php echo $rs["count"]; ?> <br>
										<strong>Địa điểm khởi hành:</strong> <?php echo $rs["place_start"]; ?> <br>
										<strong>Phương tiện:</strong><?php echo $rs["translation"]; ?><br>	
										<pre>Đảm bảo giá tốt nhất</pre>
									</p>
								</div>
								<div class="col-sm-4">
									<p><strong>Giá:</strong></p>
									<a href="bookingTour.php?id=<?php echo $rs["id"]; ?>" class="btn btn-primary">Đặt tour</a>
								</div>
								<div>
									<!-- Button trigger modal -->
									<div class="col-sm-12"><button class="btn btn-success text-capitalize mt-3" data-toggle="modal" data-target="#modalInfoTravel">lịch trình</button></div>

									<!-- Modal -->
								</div>
							</div>
						</div>
						<hr>
					<?php } 
					} ?>
					</div>
				</div>
			</div>
		</div>

		<div class="modal fade" id="modalInfoTravel" tabindex="-1" role="dialog" aria-labelledby="exampleModalLabel" aria-hidden="true">
			<div class="modal-dialog" role="document">
				<div class="modal-content">
					<div class="modal-header">
						<h5 class="modal-title" id="exampleModalLabel">Lịch trình chuyến đi</h5>
						<button type="button" class="close" data-dismiss="modal" aria-label="Close">
							<span aria-hidden="true">&times;</span>
						</button>
					</div>
					<div class="modal-body">
						<div class="row">
							<div class="row">
  <div class="col-4">
    <div class="list-group" id="list-tab" role="tablist">
      <a class="list-group-item list-group-item-action active" id="list-home-list" data-toggle="list" href="#list-home" role="tab" aria-controls="home">Thông tin chuyến đi</a>
      <a class="list-group-item list-group-item-action" id="list-profile-list" data-toggle="list" href="#list-profile" role="tab" aria-controls="profile">Giá bao gồm</a>
      <a class="list-group-item list-group-item-action" id="list-messages-list" data-toggle="list" href="#list-messages" role="tab" aria-controls="messages">Lịch trình</a>
      <a class="list-group-item list-group-item-action" id="list-settings-list" data-toggle="list" href="#list-settings" role="tab" aria-controls="settings">Chính sách</a>
    </div>
  </div>
  <div class="col-8">
    <div class="tab-content" id="nav-tabContent">
      <div class="tab-pane fade show active" id="list-home" role="tabpanel" aria-labelledby="list-home-list">
      	Tour ID: 123 <br>
      	Ngày khởi hành: Hàng ngày <br>
      	Thời gian: 1 ngày 0 đêm <br>
      	Điểm khởi hành: Khách sạn Cung Đình Huế <br>
      	Điểm đến: Đại Nội Huế <br>
      	Phương tiện: Ô tô

      </div>
      <div class="tab-pane fade" id="list-profile" role="tabpanel" aria-labelledby="list-profile-list">Xe, hướng dẫn viên, phí tham quan, nước suối</div>
      <div class="tab-pane fade" id="list-messages" role="tabpanel" aria-labelledby="list-messages-list">

      	<strong>8h30 – 9h00:</strong> Đón khách tại Khách sạn Tham quan: Vườn Hoa và Vườn Rau Vạn Thànhbr<br>	

      	<strong>9h00 – 9h30:</strong> Nuôi  Tằm<br>

      	<strong>9h45 – 10h20:</strong> Cơ sở Quấn tơ- Dệt Lụa<br>

      	<strong>10h30 – 11h30:</strong> Chùa Linh Ấn<br>

      	<strong>12h00:</strong> Đoàn Dùng cơm trưa tại thị trấn Nam Bang<br>

      	<strong>13h30 – 14h00:</strong> Nông Trại Nấm<br>

      	<strong>14h00 – 14h30:</strong> Trại nuôi Dế<br>

      	<strong>14h40 – 15h00:</strong> Cơ sở nấu rượu Gạo Nguyên Chất- Xem cách Nuôi Nuôi Chồn để sản xuất cà phê Chồn nổi tiếng - Thưởng thức cà phê nguyên chất.<br>

      	<strong>15h00 – 15h40:</strong> Thác Voi - Chùa Linh Ấn<br>
      </div>
      <div class="tab-pane fade" id="list-settings" role="tabpanel" aria-labelledby="list-settings-list"><strong>8h30:</strong> sáng hàng ngày, Đón khách tại khách sạn. <br>

      Chúng tôi cam kết đón khách đúng giờ, đúng chương trình, Không đưa khách vào các điểm ăn, uống mà giá cả không hợp lý - Trên đường đi các bạn có thể dừng chụp hình theo ý muốn - Chúng tôi phục vụ để các bạn hài lòng nhất.</div>
    </div>
  </div>
</div>
						</div>
					</div>
					<div class="modal-footer">
						<button type="button" class="btn btn-secondary" data-dismiss="modal">Close</button>
					</div>
				</div>
			</div>
		</div>
<script type="text/javascript" src="js/jquery-3.2.1.min.js"></script>
<script type="text/javascript" src="js/bootstrap.bundle.min.js"></script>
<script type="text/javascript" src="js/moment-with-locales.min.js"></script>
<script type="text/javascript" src="js/tempusdominus-bootstrap-4.min.js"></script>
<script type="text/javascript">
	$(function () {
                $('#datetimepickerTour').datetimepicker({
                	locale: 'vi'
                });
            });
</script>
</body>
