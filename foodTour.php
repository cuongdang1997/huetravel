<?php
	require_once "common/connect.php";
	//
	if(isset($_GET["id"]) && $_GET["id"] != ""){
		$stmt = $conn -> prepare("SELECT id, title, files, short_des, description FROM products WHERE id = ?");
		$stmt -> bind_param("i", $_GET["id"]);
		$stmt -> execute();
		$stmt -> bind_result($id, $title, $files, $short_des, $description);
		$stmt -> fetch();
		$stmt -> close();
	}
?>
<!DOCTYPE html>
<html>
<head>
	<title></title>
</head>
<body>
	<?php include "template/header.php";  ?>
	<div id="main"> <strong><?php echo $title; ?></strong><hr>
		<div class="row row1">
			<div class="col-md-9" ;>
				<?php echo $short_des; ?> <br>
				<img class="img-fluid img-main-introduce" id="anh" src="admin/<?php echo $files; ?>" >
				<div><?php echo $description; ?></div>
			</div>
			<div class="col-md-3">
				<h6><div id="foodTour" class="title-related-post p-3 mt-2 mb-2 rounded"><strong>Món ăn liên quan </strong></h6><hr>
					<div id="food"> 
						<div class="row place-related text-center">
							<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM products ORDER BY id LIMIT 6")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
									?>
									<div class="col-sm-12 col-md-6 col-lg-6 mb-3">
										<a href="foodTour.php?id=<?php echo $id; ?>">
											<p class="text-center"><?php echo $title ?></p>
											<img src="admin/<?php echo $files; ?>" class="img-fluid">
										</a>
									</div>
									<?php
								}
								$stmt -> close();
							}
							?>
						</div>
					</div>	
					<h6><div id="addressTour" class="title-related-post p-3 mt-2 mb-2 rounded"><strong>Địa điểm du lịch liên quan </strong></h6><hr>
					<div id="place"> 
						<div class="row place-related text-center">
							<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM news ORDER BY id LIMIT 6")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
									?>
									<div class="col-sm-12 col-md-6 col-lg-6 mb-3">
										<a href="placetour.php?id=<?php echo $id; ?>">
											<p class="text-center"><?php echo $title ?></p> 
											<img src="admin/<?php echo $files; ?>" class="img-fluid">
										</a>
									</div>
									<?php
								}
								$stmt -> close();
							}
							?>
						</div>
					</div>	
				</div>
				
			</div>
		</div>
	</div>
	<?php include "template/footer.php" ?>
</body>
</html>