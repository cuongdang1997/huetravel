<?php
	session_start();
	require_once "common/connect.php";
	require_once "../common/sysenv.php";
	//
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["submit"]) && $_POST["submit"] != ""){
			$mess = "";
			// Check if username is empty
		    if(empty(trim($_POST["user"]))){
		        $mess = "Vui lòng nhập tài khoản";
		    } else {
		        $user = trim($_POST["user"]);
		    }
		    // Check if password is empty
		    if(empty(trim($_POST["pass"]))){
		        $mess = "Vui lòng nhập mật khẩu";
		    } else {
		        $pass = trim($_POST["pass"]);
		    }
		    $stmt = $conn -> prepare($getSQL["gAdmin"]);
    		$stmt -> bind_param("s", $user);
    		$stmt -> execute();
    		$stmt -> bind_result($result_id, $result_username, $result_password, $result_role);
    		$stmt -> fetch();
    		//khi thuc hien them lenh thi tiep tuc con k thi close.
    		$stmt -> close();
    		//
    		
		    // connect in login
	    	if($mess == ""){
	    		$stmt = $conn -> prepare($getSQL["gAdmin"]);
	    		$stmt -> bind_param("s", $user);
	    		if($stmt -> execute()){
	    			$_SESSION['userData'] = array(
		    			'id' => $result_id,
		    			'username' => $result_username,
		    			'password' => $result_password,
		    			'role' => $result_role
		    		);
		    		header("Location: ../index.php");
	    		} else {
	    			$mess = "Đăng Nhập Không Thành Công";
	    		}
	    		$stmt -> close();
	    	}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
	<meta charset="UTF-8">
	<title>Administrator | Dashboard</title>
	<link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
	<link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
	<link rel="stylesheet" type="text/css" href="dist/css/style.css">
</head>
<body>
	<div class="login">
		<?php
			if(isset($mess)){
		?>
			<h4 class="text-danger text-center"><?php echo $mess; ?></h4>
		<?php
			}
		?>
		<h2 align="center">Hệ Thống Quản Trị</h2>
		<form action="login.php" method="POST" style="text-align:center;">
			<input type="text" class="form-control" placeholder="Tài Khoản" name="user"><br/><br/>
			<input type="password" class="form-control" placeholder="Mật Khẩu" name="pass"><br/><br/>
			<input type="submit" value="Đăng Nhập" name="submit">
		</form>
	</div>
</body>
</html>