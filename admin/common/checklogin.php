<?php
  session_start();
  if (!(isset($_SESSION["userData"]) && $_SESSION["userData"]["role"] == 0)) {
  	header("location: login.php");
  }
?>
