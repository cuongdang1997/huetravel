<?php
require_once 'connect.php';
require_once '../../common/sysenv.php';

if($_SERVER['REQUEST_METHOD'] == 'GET' && isset($_GET['func'])){

	$conn = new PDO("mysql:host=$host;dbname=$dbname;charset=utf8", $username, $password);

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);

	$sqlQuery = $getSQL[$_GET['func']];

	if (strpos($sqlQuery, '1=1')){

			$condition = "1=1";

			foreach ($_GET as $name => $value) {

				if($name != "func" && $value != null && $value != ""){

					//$condition .= " and ".$name."='".$value."'";
					$condition .= " and ".$name."=:".$name;
					
				}
				
			}	

			$sqlQuery = str_replace("1=1",$condition,$sqlQuery);
	}

	$stmt = $conn->prepare($sqlQuery);

	foreach ($_GET as $name => $value) {

		if($name != "func" && $value != null && $value != "") {	

			$stmt->bindValue(':'.$name, $value);

		}

	} 	
	
	$stmt->execute();

	$result = $stmt->fetchAll();

	$stmt->closeCursor();
	
	$stmt = null;

	$conn = null;

	$json = array();

	foreach ($result as $value) {			

			array_push($json, $value);
		}
		
	print json_encode($json);

	die();

}else{
	print json_encode(0);

	die();	
}

?>