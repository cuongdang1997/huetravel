<?php
require_once 'connect.inc.php';
require_once 'sysenv.php';

if($_SERVER['REQUEST_METHOD'] == 'POST' && isset($_POST['func'])){

	$conn = new PDO("mysql:host=$host;dbname=$dbname", $username, $password);

	$conn->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
	
	$sqlQuery = $getSQL[$_POST['func']];

	$stmt = $conn->prepare($sqlQuery);

	foreach ($_POST as $name => $value) {

		if($name != "func" && $name != null && $name != "") {	

			$stmt->bindValue(':'.$name, $value);

		}

	} 

	$stmt->execute();

	$stmt->closeCursor();

	$stmt = null; 

	$conn = null;

	print json_encode(0);

	die();
}

?>