<?php
require_once "common/connect.php";
require_once "../common/sysenv.php";
//get post in by ID
if (isset($_GET["id"])) {
    $id = $_GET["id"];
    $stmt = $conn -> prepare($getSQL["getMenuById"]);
    $stmt -> bind_param("i", $id);
    $stmt -> execute(); 
    $stmt -> bind_result($result_id, $result_name, $result_sort_order, $result_status, $result_description);
    $stmt -> fetch();
    $stmt -> close();
}
if ($_SERVER["REQUEST_METHOD"] == "POST") {
    if(isset($_POST["submit"])){
          $mess = "";
            //
          $id = $_GET["id"];
          $name = $_POST["name"];
          $sort_order = $_POST["sort_order"];
          $status = $_POST["status"];
          $description = $_POST["description"];
            //
          if($name == "" || $description == "" || $sort_order == "" || $status == ""){
            $mess = "Vui lòng nhập đầy đủ thông tin";
          }
            //
          if($mess == ""){
            $stmt = $conn -> prepare($getSQL["editMenu"]);
            $stmt -> bind_param("siisi", $name, $sort_order, $status, $description, $id);
            if($stmt -> execute()){
              $messSuccess = "Sửa thành công";
            } else {
              $mess = "Sửa thất bại";
            }
            $stmt -> close();
            header("Location: show_thongtin.php");
          }
        }
      }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <script src="ckeditor/ckeditor.js"></script>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
  <!-- <link rel="stylesheet" type="text/css" href="dist/css/style.css"> -->
     </head>
     <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">
        <!-- include header -->
        <?php include "includes/header.php"; ?>
        <!-- /.include header -->        
        <!-- include sidebar -->
        <?php include "includes/sidebar.php"; ?>
        <!-- /.include sidebar -->
        <div class="content-wrapper">
          <section class="content-header">
            <div class="container-fluid">
              <!-- menu service cha -->
              <div class="panel-group">
                <div class="panel panel-info">
                  <h4 class="panel-heading text-center text-uppercase">Sửa Thông Tin: <?php echo $result_name; ?></h4>
                  <div class="panel-body">
                    <?php if(isset($messFalse)){ echo "<h5 class='text-center text-danger'>".$messFalse."</h5>"; } ?>
                    <?php if(isset($messSuccess)){ echo "<h5 class='text-center text-success'>".$messSuccess."</h5>"; } ?>
                    <form method="POST">
                      <div class="row">
                        <div class="col-md-6" style="margin-bottom: 10px;">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="name">Tên địa điểm</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" name="name" value="<?php echo $result_name; ?>" required>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="margin-bottom: 10px;">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="sort_order">Thứ tự ưu tiên</label>
                            <div class="col-sm-6">
                              <input type="number" name="sort_order" class="form-control" value="<?php echo $result_sort_order; ?>" required>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-md-6" style="margin-bottom: 10px;">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="status">Tình trạng</label>
                            <div class="col-sm-6">
                              <select class="form-control" name="status">
                                <option value="0">Đang hoạt động</option>
                                <option value="1" <?php if ($result_status == '1') echo 'selected';?>>Ngừng hoạt động</option>
                              </select>
                            </div>
                          </div>
                        </div>
                        <div class="col-md-6" style="margin-bottom: 10px;">
                          <div class="form-group">
                            <label class="control-label col-sm-4" for="description">Mô tả</label>
                            <div class="col-sm-6">
                              <input type="text" class="form-control" name="description" value="<?php echo $result_description; ?>" required>
                            </div>
                          </div>
                        </div>
                      </div>
                      <div class="form-group text-center">
                        <button type="submit" name="submit" class="btn btn-success">Lưu</button>
                        <button class="btn btn-danger"><a href="show_thongtin.php" class="textInfo">Quay Lại</a></button>
                      </div>
                    </form>
                  </div>
                </div>
              </div>
            </div>
          </section>
          <!-- Content Wrapper. Contains page content -->
          <!-- insert into menu() -->
          <!-- /.content -->
        </div>
        <!-- /.content-wrapper -->

        <!-- include footer -->
        <?php include "includes/footer.html"; ?>
        <!-- /.include footer -->
      </div>
      <!-- ./wrapper -->

      <!-- jQuery 3 -->
      <script src="bower_components/jquery/dist/jquery.min.js"></script>
      <!-- jQuery UI 1.11.4 -->
      <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
      <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
      <script>
        $.widget.bridge('uibutton', $.ui.button);
      </script>
      <!-- Bootstrap 3.3.7 -->
      <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
      <!-- Morris.js charts -->
      <script src="bower_components/raphael/raphael.min.js"></script>
      <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
      <!-- Sparkline -->
      <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
      <!-- jvectormap -->
      <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
      <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
      <!-- jQuery Knob Chart -->
      <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
      <!-- daterangepicker -->
      <script src="bower_components/moment/min/moment.min.js"></script>
      <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
      <!-- datepicker -->
      <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
      <!-- Bootstrap WYSIHTML5 -->
      <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
      <!-- Slimscroll -->
      <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
      <!-- FastClick -->
      <script src="bower_components/fastclick/lib/fastclick.js"></script>
      <!-- AdminLTE App -->
      <script src="dist/js/adminlte.min.js"></script>
      <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
      <!-- <script src="dist/js/pages/dashboard.js"></script> -->
      <!-- AdminLTE for demo purposes -->
      <script src="dist/js/demo.js"></script>
    </body>
  </html>