<?php
require_once "common/connect.php";
require_once "../common/sysenv.php";
  //
if($_SERVER["REQUEST_METHOD"] == "POST") { 
  if(isset($_POST["submit"])){

    $mess = "";
    //
    $title = $_POST["title"]; 
    $description = $_POST["description"];
    $short_des = $_POST["short_des"];
    $sort_order = $_POST["sort_order"];
    //
    if($title == "" || $description == "" || $short_des == "" || $sort_order == ""){
       $mess = "Vui lòng nhập đầy đủ thông tin";
    }

    //
    if($mess == ""){
           //
      $error = array();
      $target_dir = "uploads/";
      //tạo đường dẫn file sau khi upload lên hệ thống    
      $target_file = $target_dir.basename($_FILES['fileToUpload']['name']);

      //Kiểm tra kiểu file hợp lệ
      $type_file = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);

      $type_file_allow = array('png', 'jpg', 'jpeg', 'gif');
      if(!in_array(strtolower($type_file), $type_file_allow)){
          $error['fileToUpload'] = "File bạn vừa chọn hệ thống không hỗ trợ, bạn vui lòng chọn hình ảnh khác";
      }
      //Kiểm tra kích thước file
      $size_file = $_FILES['fileToUpload']['size'];
      if ($size_file > 1000000) {
          $error['fileToUpload'] = "File của bạn chọn không được quá 1000000";
      }
      // Kiểm tra đã tồn tại trong hệ thống
      if(file_exists($target_file)){
          $error['fileToUpload'] = "File bạn chọn đã tồn tại trên hệ thống";
      }
      //
      if(empty($error)){
          if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
              $flag = true;
          }
      }
      //
      $stmt = $conn -> prepare($getSQL["iNews"]);
      $stmt -> bind_param("ssssi", $title, $description, $target_file, $short_des, $sort_order); 
      if($stmt -> execute()){
        $messSuccess = "Thêm mới thành công";
      } else {
        $mess = "Thêm mới thất bại";
      }
      $stmt -> close();
    }
  }
}
?> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 <!--  <link rel="stylesheet" type="text/css" href="dist/css/style.css"> -->

     </head>
     <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">

        <!-- include header -->
        <?php include "includes/header.php"; ?>
        <!-- /.include header -->
        
        <!-- include sidebar -->
        <?php include "includes/sidebar.php"; ?>
        <!-- /.include sidebar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
        <div class="container-fluid">
       
          <!-- menu service cha -->
          <div class="panel-group">
            <div class="panel panel-info">
              <div class="panel-heading text-center "><h4><strong>THÊM MỚI ĐỊA ĐIỂM</strong></h4></div>
              <div class="panel-body">
                <?php if(isset($messFalse)){ echo "<h5 class='text-center text-danger'>".$messFalse."</h5>"; } ?>
                <?php if(isset($messSuccess)){ echo "<h5 class='text-center text-success'>".$messSuccess."</h5>"; } ?>
                <form class="form-horizontal" method="POST" enctype="multipart/form-data">  
            <div class="row">
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4">Chọn Thông Tin</label>
                  <div class="col-sm-7">          
                    <select class="form-control">
                      <option>-- Chọn thông tin --</option>
                      <option value="0">Địa Điểm Du Lịch</option>
                      <option value="1">Ẩm Thực Huế</option>
                      <option value="2">Tour</option>
                      <button class="btn btn-primary text-center">
                      <a href="add_diadiem.php" class="textInfo">Thêm Thông Tin</a>
                      </button> 
                      <option></option>
                    </select>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="title">Tiêu Đề</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="title">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="control-label col-sm-4" for="sort_order">Thứ tự ưu tiên</label>
                  <div class="col-sm-7">
                    <input type="number" class="form-control" name="sort_order" placeholder="Thứ tự ưu tiên" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="short_des">Mô tả ngắn</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="short_des">
                  </div>
                </div>
              </div>
            </div>
            <div class="row">
              <div class="col-md-12">
                <div class="form-group row">
                  <label class="control-label col-sm-6" for="files">Chọn ảnh địa điểm du lịch</label>
                  <!-- attribute "for" of label to focus input by "id"-->
                   <input type="file" name="fileToUpload" id="files"/>
                </div>
              </div>
            </div>
            <div class="row">
              <label class="control-label col-sm-2" for="description">Nội dung</label>
                <div class="col-sm-9">
                  <textarea type="text" class="form-control" rows="13" name="description"></textarea>
                </div>
            </div>
            <br>
            <div class="form-group text-center">
              <button type="submit" name="submit" class="btn btn-success">Lưu</button>
              <button type="reset" class="btn btn-primary">Nhập Lại</button>
              <button class="btn btn-danger"><a href="show_diadiem.php" class="textInfo">Quay Lại</a></button>
            </div>
          </div>
        </form>
              </div>
            </div>
          </div>
        </div>
      </section>

          <!-- Content Header (Page header) -->
          
          <!-- Main content -->
          
        <!-- insert into menu() -->
        <!-- /.content -->
      <!-- /.content-wrapper -->

      <!-- include footer -->
      <?php include "includes/footer.html"; ?>
      <!-- /.include footer -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!--  -->
    <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
    <!--  -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="bower_components/raphael/raphael.min.js"></script>
    <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
    <!-- Sparkline -->
    <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="dist/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
      CKEDITOR.replace('description');
    </script>
  </body>
  </html>