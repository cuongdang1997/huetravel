<?php
	require_once "common/checklogin.php";
	require_once "common/connect.php";
	require_once "../common/sysenv.php";
	//insert menu cha
 	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["save"])){
			//
			$messFalse = "";
			//
			$name = $_POST["name"];
			$status = 1;
			//
			if($name == ""){
				$messFalse = "Vui lòng nhập đầy đủ thông tin";
			}
			//
			if($messFalse == ""){
				$stmt = $conn -> prepare($getSQL["iMenuService"]);
				$stmt -> bind_param("si", $name, $status);
				if($stmt -> execute()){
					// $messSuccess = "Lưu thành công";
				header("Location: list_service.php");exit();
				} else {
					$messFalse = "Lưu thất bại";
				}
				$stmt -> close();
			}
		}
	}
	//insert menu con
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["saveSubMenuService"])){
			//
			$messFalseSubMenuService = "";
			//
			$selectMenuService = $_POST["selectMenuService"];
			$name = $_POST["name"];
			//
			if($selectMenuService == "" || $name == ""){
				$messFalseSubMenuService = "Vui lòng nhập đầy đủ thông tin";
			}
			//
			if($messFalseSubMenuService == ""){
				$stmt = $conn -> prepare($getSQL["iSubMenuService"]);
				$stmt -> bind_param("is", $selectMenuService, $name);
				if($stmt -> execute()){
					// $messSuccessSubMenuService = "Lưu thành công";
					header("Location: list_service.php");exit();
				} else {
					$messFalseSubMenuService = "Lưu thất bại";
				}
				$stmt -> close();
			}
		}
	}
	//insert service
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["saveService"])){
			//
			$messFalseService = "";
			//
			$selectService = $_POST["selectService"];
      $selectSubMenuService = $_POST["selectSubMenuService"];
      $price  = $_POST["price"];
      $description = $_POST["description"];
			//
			if($selectService == ""){
				$messFalseService = "Vui lòng nhập đầy đủ thông tin";
			}
      //
      if($messFalseService == ""){
           //
      $error = array();
      $target_dir = "uploads/";
      //tạo đường dẫn file sau khi upload lên hệ thống    
      $target_file = $target_dir.basename($_FILES['fileToUpload']['name']);

      //Kiểm tra kiểu file hợp lệ
      $type_file = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);

      $type_file_allow = array('png', 'jpg', 'jpeg', 'gif');
      if(!in_array(strtolower($type_file), $type_file_allow)){
          $error['fileToUpload'] = "File bạn vừa chọn hệ thống không hỗ trợ, bạn vui lòng chọn hình ảnh khác";
      }
      //Kiểm tra kích thước file
      $size_file = $_FILES['fileToUpload']['size'];
      if ($size_file > 1000000) {
          $error['fileToUpload'] = "File của bạn chọn không được quá 1000000";
      }
      // Kiểm tra đã tồn tại trong hệ thống
      if(file_exists($target_file)){
          $error['fileToUpload'] = "File bạn chọn đã tồn tại trên hệ thống";
      }
      //
      if(empty($error)){
          if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
              $flag = true;
          }
      }
      //
      $stmt = $conn -> prepare($getSQL["iSubService"]);
        $stmt -> bind_param("iiiss", $selectService, $selectSubMenuService, $price, $target_files, $description);
        if($stmt -> execute()){
          // $messSuccessSubMenuService = "Lưu thành công";
          header("Location: list_service.php");exit();
        } else {
          $messFalseService = "Lưu thất bại";
        }
        $stmt -> close();
    }
			// if($messFalseService == ""){
			// 	$stmt = $conn -> prepare()
			// }
      // if($messFalseService == ""){
      //   $stmt = $conn -> prepare($getSQL["iSubService"]);
      //   $stmt -> bind_param("iiis", $selectService, $selectSubMenuService, $price, $description);
      //   if($stmt -> execute()){
      //     // $messSuccessSubMenuService = "Lưu thành công";
      //     header("Location: list_service.php");exit();
      //   } else {
      //     $messFalseService = "Lưu thất bại";
      //   }
      //   $stmt -> close();
      // }
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head> 
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- include header -->
    <?php include "includes/header.php"; ?>
    <!-- /.include header -->
    
    <!-- include sidebar -->
    <?php include "includes/sidebar.php"; ?>
    <!-- /.include sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
      	<div class="container-fluid">
      		<h3 class="text-center text-uppercase">Quản Lý Dịch Vụ</h3>
      		<!-- menu service cha -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center ">Danh Mục Dịch Vụ</div>
      				<div class="panel-body">
      					<?php if(isset($messFalse)){ echo "<h5 class='text-center text-danger'>".$messFalse."</h5>"; } ?>
      					<?php if(isset($messSuccess)){ echo "<h5 class='text-center text-success'>".$messSuccess."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-5">
      								<div class="form-group">
      									<label>Tên Danh Mục Dịch Vụ</label>
      									<input type="text" name="name" class="form-control" required>
      								</div>
      							</div>
      							<div class="col-md-5">
       							</div>
      							<div class="col-md-2 text-center">
      								<div class="form-group">
      									<label></label>
      									<button class="btn btn-success" name="save" value="save"><i class="fa fa-save"></i> Lưu</button>
      								</div>
      							</div>
      						</div>
      					</form>
      				</div>
      			</div>
      		</div>
      		<!-- menu service con -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center ">Thông Tin Dịch Vụ</div>
      				<div class="panel-body">
      					<?php if(isset($messFalseSubMenuService)){ echo "<h5 class='text-center text-danger'>".$messFalseSubMenuService."</h5>"; } ?>
      					<?php if(isset($messSuccessSubMenuService)){ echo "<h5 class='text-center text-success'>".$messSuccessSubMenuService."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-5">
      								<div class="form-group">
      								<label>Chọn Thông Tin Dịch Vụ</label>
      								<select class="form-control" name="selectMenuService" required>
      									<option value="">--Chọn Thông Tin--</option>
      									<?php
      										if($stmt = $conn -> prepare($getSQL["gMenuService"])){
      											$statusMenuServiceId = 1;
      											$stmt -> bind_param("i", $statusMenuServiceId);
      											$stmt -> execute();
      											$stmt -> bind_result($result_menu_service_id, $result_menu_service_name);
      											while($stmt -> fetch()){
      									?>
      										<option value="<?php if(isset($result_menu_service_id)){ echo $result_menu_service_id; } ?>"><?php if(isset($result_menu_service_name)){ echo $result_menu_service_name; } ?></option>
      									<?php
      											}
      											$stmt -> close();
      										}
      									?>
      								</select>
      								</div>
      							</div>
      							<div class="col-md-5">
      								<div class="form-group">
      								<label>Tên Thông Tin Dịch Vụ</label>
      								<input type="text" name="name" class="form-control" required>
      								</div>
      							</div>
      							<div class="col-md-2 text-center">
      								<div class="form-group">
      									<label></label>
      									<button class="btn btn-success" name="saveSubMenuService" value="saveSubMenuService"><i class="fa fa-save"></i> Lưu</button>
      								</div>
      							</div>
	      					</div>
      					</form>
      				</div>
      			</div>
      		</div>
      		<!-- Service -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center ">Dịch Vụ</div>
      				<div class="panel-body">
      					<?php if(isset($messFalseService)){ echo "<h5 class='text-center text-danger'>".$messFalseService."</h5>"; } ?>
      					<?php if(isset($messSuccessSubMenuService)){ echo "<h5 class='text-center text-success'>".$messSuccessSubMenuService."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-5">
      								<div class="form-group">
      								<label>Chọn Thông Tin Dịch Vụ</label>
      								<select class="form-control" id="menuSelect" name="selectService" required>
      									<option value="">--Chọn Thông Tin--</option>
      									<?php
      										if($stmt = $conn -> prepare($getSQL["gMenuService"])){
      											$stmt -> execute();
      											$stmt -> bind_result($result_menu_service_id, $result_menu_service_name);
                            while($stmt -> fetch()){
                        ?>
                          <option value="<?php if(isset($result_menu_service_id)){ echo $result_menu_service_id; } ?>"><?php if(isset($result_menu_service_name)){ echo $result_menu_service_name; } ?></option>
                        <?php
                            }
                            $stmt -> close();
                          }
      									?>
      								</select>
      								</div>
      							</div>
      							<div class="col-md-5">
      								<div class="form-group">
      								<label>Chọn tên thông tin dịch vụ</label>
                      <select class="form-control" id="subMenuSelect" name="selectSubMenuService" required>
                        <option value="">--Chọn Thông Tin--</option>
                      </select>
      								</div>
      							</div>
                    <div class="col-md-2 text-center">
                      <div class="form-group">
                        <label></label>
                        <button class="btn btn-success" name="saveService" value="saveService"><i class="fa fa-save"></i> Lưu</button>
                      </div>
                    </div>
	      					</div>
	      					<div class="row">
	      						<div class="col-md-5">
      								<div class="form-group">
      								<label>Giá</label>
      								<input type="number" name="price" class="form-control" required>
      								</div>
      							</div>
      							<div class="col-md-5">
      								<div class="form-group">
      								<label>Ảnh đại diện</label>
      								 <input type="file" name="fileToUpload" id="files"/>
      								</div>
      							</div>      							
	      					</div>
	      					<div class="row">
      							<div class="col-md-10">
      								<div class="form-group">
      								<label>Mô tả</label>
      								<textarea type="text" class="form-control" rows="13" name="description"></textarea>
      								</div>
      							</div>
	      					</div>
      					</form>
      				</div>
      			</div>
      		</div>
		</div>
      </section>
      <!-- Main content -->

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    <!-- include footer -->
    <?php include "includes/footer.html"; ?>
    <!-- /.include footer -->
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="bower_components/raphael/raphael.min.js"></script>
  <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="dist/js/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
  <script>
      CKEDITOR.replace('description');
      //
      $(document).ready(function(){
        $("#menuSelect").change(function(e){
          if($("#menuSelect").val()){
            var datathis = {
              func: "gMenuSelectService",
              id: $("#menuSelect").val()
            }
            //
            $.ajax({
              type: "GET",
              data: datathis,
              url: "common/get.php",
              success: function(data){
                var data = JSON.parse(data);
                if(data.length > 0){
                  $("#subMenuSelect").empty();
                  $.each(data, function(key, value){
                    $("#subMenuSelect").append("<option value="+value.ms2Id+">"+value.ms2Name+"</option>");
                  });
                }
              }
            });
          } else {
          $("#subMenuSelect").empty();
          }
        });
      });
  </script>
    <!-- <script>
    function getSubMenu(str) {
      if (str=="") {
        document.getElementById("subMenu").innerHTML="";
        return;
      }
      if (window.XMLHttpRequest) {
        // code for IE7+, Firefox, Chrome, Opera, Safari
        xmlhttp=new XMLHttpRequest();
      } else { // code for IE6, IE5
        xmlhttp=new ActiveXObject("Microsoft.XMLHTTP");
      }
      xmlhttp.onreadystatechange=function() {
        if (this.readyState==4 && this.status==200) {
          document.getElementById("subMenu").innerHTML=this.responseText;
        }
      }
      xmlhttp.open("GET","get_service.php?id="+str,true);
      xmlhttp.send();
    }
</script> -->
</body>
</html>