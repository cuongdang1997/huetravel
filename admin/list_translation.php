<?php
	require_once "common/checklogin.php";
	require_once "common/connect.php";
	require_once "../common/sysenv.php";

  $result = $conn->query($getSQL["getAllTranslation"]);
	//insert menu cha
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["save"])){
			//
			$messFalse = "";
			//
			$name = $_POST["name"];
			$status = 1;
			//
			if($name == ""){
				$messFalse = "Vui lòng nhập đầy đủ thông tin";
			}
			//
			if($messFalse == ""){
				$stmt = $conn -> prepare($getSQL["iMenuTranslation"]);
				$stmt -> bind_param("si", $name, $status);
				if($stmt -> execute()){
					// $messSuccess = "Lưu thành công";
					header("Location: list_translation.php");exit();
				} else {
					$messFalse = "Lưu thất bại";
				}
				$stmt -> close();
			}
		}
	}
	//insert menu con
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["saveSubMenuTranslation"])){
			//
			$messFalseSubMenuTranslation = "";
			//
			$selectMenuTranslation = $_POST["selectMenuTranslation"];
			$note = $_POST["note"];
			//
			if($selectMenuTranslation == "" || $note == ""){
				$messFalseSubMenuTranslation = "Vui lòng nhập đầy đủ thông tin";
			}
			//
			if($messFalseSubMenuTranslation == ""){
				$stmt = $conn -> prepare($getSQL["iSubMenuTranslation"]);
				$stmt -> bind_param("is", $selectMenuTranslation, $note);
				if($stmt -> execute()){
					// $messSuccessSubMenuTranslation = "Lưu thành công";
					header("Location: list_translation.php");exit();
				} else {
					$messFalseSubMenuTranslation = "Lưu thất bại";
				}
				$stmt -> close();
			}
		}
	}
	//insert translation
	// if($_SERVER["REQUEST_METHOD"] == "POST"){
	// 	if(isset($_POST["saveTranslation"])){
	// 		//
	// 		$messFalseTranslation = "";
	// 		//
	// 		$selectTranslation = $_POST["selectTranslation"];
	// 		$name = $_POST["name"];
	// 		//
	// 		if($selectTranslation == "" || $name == ||){
	// 			$messFalseTranslation = "Vui lòng nhập đầy đủ thông tin";
	// 		}
	// 		if($messFalseTranslation == ""){
	// 			$stmt = $conn -> prepare()
	// 		}
	// 	}
	// }
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head> 
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- include header -->
    <?php include "includes/header.php"; ?>
    <!-- /.include header -->
    
    <!-- include sidebar -->
    <?php include "includes/sidebar.php"; ?>
    <!-- /.include sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
      	<div class="container-fluid">
      		<h3 class="text-center text-uppercase">Quản Lý Phương Tiện</h3>
      		<!-- menu translation cha -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center ">Thêm Loại Phương Tiện</div>
      				<div class="panel-body">
      					<?php if(isset($messFalse)){ echo "<h5 class='text-center text-danger'>".$messFalse."</h5>"; } ?>
      					<?php if(isset($messSuccess)){ echo "<h5 class='text-center text-success'>".$messSuccess."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-4">
      								<div class="form-group">
      									<label>Tên Loại Phương Tiện</label>
      									<input type="text" name="name" class="form-control" required>
      								</div>
      							</div>
      							<div class="col-md-4">
       							</div>
      							<div class="col-md-4 text-center">
      								<div class="form-group">
      									<label></label>
      									<button class="btn btn-success" name="save" value="save"><i class="fa fa-save"></i> Lưu</button>
      								</div>
      							</div>
      						</div>
      					</form>
      				</div>
      			</div>
      		</div>
        		<!-- menu translation con -->
        		
      		<!-- Translation -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center">Phương Tiện</div>
      				<div class="panel-body">
      					<?php if(isset($messFalseSubMenuTranslation)){ echo "<h5 class='text-center text-danger'>".$messFalseSubMenuTranslation."</h5>"; } ?>
      					<?php if(isset($messSuccessSubMenuTranslation)){ echo "<h5 class='text-center text-success'>".$messSuccessSubMenuTranslation."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-4">
      								<div class="form-group">
      								<label>Chọn Loại Phương Tiện</label>
      								<select class="form-control" name="selectMenuTranslation" required>
      									<option value="">--Chọn Thông Tin--</option>
      									<?php
      										if($stmt = $conn -> prepare($getSQL["gMenuTranslation"])){
      											$statusMenuTranslationId = 1;
      											$stmt -> bind_param("i", $statusMenuTranslationId);
      											$stmt -> execute();
      											$stmt -> bind_result($result_menu_translation_id, $result_menu_translation_name);
      											while($stmt -> fetch()){
      									?>
      										<option value="<?php if(isset($result_menu_translation_id)){ echo $result_menu_translation_id; } ?>"><?php if(isset($result_menu_translation_name)){ echo $result_menu_translation_name; } ?></option>
      									<?php
      											}
      											$stmt -> close();
      										}
      									?>
      								</select>
      								</div>
      							</div>
      							<div class="col-md-4">
      								<div class="form-group">
      								<label>Ghi chú</label>
      								<input type="text" name="note" class="form-control" required>
      								</div>
      							</div>
                    <div class="col-md-4">
                      <div class="form-group text-center">
                        <label></label>
                        <button class="btn btn-success" name="saveSubMenuTranslation" value="saveSubMenuTranslation"><i class="fa fa-save"></i> Lưu</button>
                      </div>
                    </div>
                  </div>
	      					<div class="row">
	      						<div class="col-md-4">
      								<div class="form-group">
      								<label>Giá</label>
      								<input type="number" name="price" class="form-control" required>
      								</div>
      							</div>
	      					</div>
	      				</form>
      				</div>
      			</div>
      		</div>
		</div>
      </section>
      <!-- Main content -->
       <section class="content-header">
  <div class="container-fluid">
    <!-- Customer -->
    <div class="panel-group">
      <div class="panel panel-info">
        <div class="panel-heading text-center">Danh sách phương tiện</div>
        <div class="panel-body">
          <table class="table table-striped table-bordered">
            <thead>
              <tr>
                <th>STT</th>
                <th>Tên phương tiện</th>
                <th>Mô tả</th>
                <th>Giá</th>
                <th>Thao tác</th>
              </tr>
            </thead>
            <tbody>
              <?php 
                if ($result->num_rows > 0) {
                  while($rs = $result->fetch_assoc()) {
                    echo "<tr>";
                    echo "<td>".$rs["id"]."</td>";
                    echo "<td>".$rs["name"]."</td>";
                    echo "<td>".$rs["description"]."</td>";
                    echo "<td>".$rs["price"]."</td>";
                    echo "<td><a class='btn btn-primary mr-2' href='edit_translation.php?id=".$rs["id"]."'>Sửa</a> <a class='btn btn-danger' href='delete_translation.php?id=".$rs["id"]."'>Xóa</a></td>";
                    echo "</tr>";
                  }
                }
              ?>
            </tbody>
          </table>
        </div> 
      </div>
    </div>
  </div>
</section>

      <!-- /.content -->
    </div>
    <!-- /.content-wrapper -->
    
    <!-- include footer -->
    <?php include "includes/footer.html"; ?>
    <!-- /.include footer -->
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="bower_components/raphael/raphael.min.js"></script>
  <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="dist/js/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
   <script>
      CKEDITOR.replace('description');
    </script>
</body>
</html>