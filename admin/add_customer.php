<?php
require_once "common/connect.php";
require_once "../common/sysenv.php";
  //
if($_SERVER["REQUEST_METHOD"] == "POST") { 
  if(isset($_POST["submit"])){

    $mess = "";
    //
    $name = $_POST["name"];
    $gender = $_POST["gender"];
    $dob = $_POST["dob"];
    $address = $_POST["address"];
    $phone = $_POST["phone"];
    $gmail = $_POST["gmail"];
    //
    if($name == "" || $gender == "" || $dob == "" || $phone == ""){
       $mess = "Vui lòng nhập đầy đủ thông tin";
    }

    //
    if($mess == ""){
           //
      $error = array();  
      //
      $stmt = $conn -> prepare($getSQL["iCustomer"]);
      $stmt -> bind_param("sissis", $name, $gender, $dob, $address, $phone, $gmail); 
      var_dump();
      if($stmt -> execute()){
        $messSuccess = "Thêm mới thành công";
      } else {
        $mess = "Thêm mới thất bại";
      }
      $stmt -> close();
    }
  }
}
?> 
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
  folder instead of downloading all of them to reduce the load. -->
  <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
  <!-- Morris chart -->
  <link rel="stylesheet" href="bower_components/morris.js/morris.css">
  <!-- jvectormap -->
  <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
  <!-- Date Picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
  <!-- Daterange picker -->
  <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
  <!-- bootstrap wysihtml5 - text editor -->
  <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
  <!-- Google Font -->
  <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 <!--  <link rel="stylesheet" type="text/css" href="dist/css/style.css"> -->

     </head>
     <body class="hold-transition skin-blue sidebar-mini">
      <div class="wrapper">

        <!-- include header -->
        <?php include "includes/header.php"; ?>
        <!-- /.include header -->
        
        <!-- include sidebar -->
        <?php include "includes/sidebar.php"; ?>
        <!-- /.include sidebar -->

        <!-- Content Wrapper. Contains page content -->
        <div class="content-wrapper">
          <!-- Content Header (Page header) -->
          <section class="content-header">
            <h1 class="text-center">
              Thêm Mới Khách Hàng
            </h1>
          </section>
          <br>
          <!-- Main content -->
          <form class="form-horizontal" method="POST" enctype="multipart/form-data">
            <?php if(isset($messSuccess)){ echo "<h5 class='text-success text-center'>".$messSuccess."</h5>"; } ?>
            <?php if(isset($mess)){ echo "<h5 class='text-danger text-center'>".$mess."</h5>"; } ?>
             <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="control-label col-sm-4" for="name">Tên khách hàng</label>
                  <div class="col-sm-7">
                    <input type="text" class="form-control" name="name" placeholder="Tên khách hàng" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="dob">Ngày sinh</label>
                  <div class="col-sm-6">
                    <input type="date" class="form-control" name="dob">
                  </div>
                </div>
              </div>
            </div>
           <div class="row">
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="control-label col-sm-4" for="gender">Giới tính</label>

                  <div class="col-sm-7">
                     <select type="float" class="form-control" name="gender" placeholder="Giới tính" required>
                    <option>Nam</option>
                    <option>Nữ</option>
                    <option>Khác</option>
                  </select>
                    
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="address">Địa chỉ</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="address">
                  </div>
                </div>
              </div>
            </div>
            <div class="row"> 
              <div class="col-md-6">
                <div class="form-group row">
                  <label class="control-label col-sm-4" for="phone">Số điện thoại</label>
                  <div class="col-sm-7">
                    <input type="number" class="form-control" name="phone" placeholder="Số điện thoại" required>
                  </div>
                </div>
              </div>
              <div class="col-md-6">
                <div class="form-group">
                  <label class="control-label col-sm-4" for="gmail">Gmail</label>
                  <div class="col-sm-6">
                    <input type="text" class="form-control" name="gmail">
                  </div>
                </div>
              </div>
            </div>
            <br>
            <div class="form-group text-center">
              <button type="submit" name="submit" class="btn btn-success">Lưu</button>
              <button type="reset" class="btn btn-primary">Nhập Lại</button>
              <button class="btn btn-danger"><a href="show_customer.php" class="textInfo">Quay Lại</a></button>
            </div>
          </div>
        </form>
        <!-- insert into menu() -->
        <!-- /.content -->
      </div>
      <!-- /.content-wrapper -->

      <!-- include footer -->
      <?php include "includes/footer.html"; ?>
      <!-- /.include footer -->
    </div>
    <!-- ./wrapper -->

    <!-- jQuery 3 -->
    <script src="bower_components/jquery/dist/jquery.min.js"></script>
    <!-- jQuery UI 1.11.4 -->
    <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
    <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
    <!--  -->
    <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
    <!--  -->
    <script>
      $.widget.bridge('uibutton', $.ui.button);
    </script>
    <!-- Bootstrap 3.3.7 -->
    <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
    <!-- Morris.js charts -->
    <script src="bower_components/raphael/raphael.min.js"></script>
    <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
    <!-- Sparkline -->
    <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
    <!-- jvectormap -->
    <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
    <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
    <!-- jQuery Knob Chart -->
    <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
    <!-- daterangepicker -->
    <script src="bower_components/moment/min/moment.min.js"></script>
    <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
    <!-- datepicker -->
    <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
    <!-- Bootstrap WYSIHTML5 -->
    <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
    <!-- Slimscroll -->
    <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
    <!-- FastClick -->
    <script src="bower_components/fastclick/lib/fastclick.js"></script>
    <!-- AdminLTE App -->
    <script src="dist/js/adminlte.min.js"></script>
    <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
    <!-- <script src="dist/js/pages/dashboard.js"></script> -->
    <!-- AdminLTE for demo purposes -->
    <script src="dist/js/demo.js"></script>
    <script>
      CKEDITOR.replace('description');
    </script>
  </body>
  </html>