<!-- Left side column. contains the logo and sidebar -->
<aside class="main-sidebar">
  <!-- sidebar: style can be found in sidebar.less -->
  <section class="sidebar">
    <!-- Sidebar user panel -->
    <div class="user-panel">
      <div class="pull-left image">
        <img src="dist/img/icon-admin.jpg" class="img-circle" alt="User Image">
      </div>
      <div class="pull-left info">
        <p>Admin</p>
      </div>
    </div>
    <!-- sidebar menu: : style can be found in sidebar.less -->
    <ul class="sidebar-menu" data-widget="tree">
      <li class="header">MAIN NAVIGATION</li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Thông Tin Chung</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="show_thongtin.php"><i class="fa fa-circle-o"></i>Hiển thị thông tin</a></li>
          <li><a href="show_diadiem.php"><i class="fa fa-circle-o"></i>Địa điểm du lịch</a></li>
          <li><a href="show_amthuc.php"><i class="fa fa-circle-o"></i>Ẩm thực Huế</a></li>
        </ul>
      </li>
      <li class="treeview">
        <a href="#">
          <i class="fa fa-folder"></i> <span>Thông Tin Tour</span>
          <span class="pull-right-container">
            <i class="fa fa-angle-left pull-right"></i>
          </span>
        </a>
        <ul class="treeview-menu">
          <li><a href="list_service.php"><i class="fa fa-circle-o"></i> Dịch Vụ</a></li>
          <li><a href="list_translation.php"><i class="fa fa-circle-o"></i> Phương tiện</a></li>
          <li><a href="list_bookingtour.php"><i class="fa fa-circle-o"></i> Đặt tour</a></li>
          <li><a href="list_contract.php"><i class="fa fa-circle-o"></i> Hợp đồng</a></li>
          <li><a href="list_hotel.php"><i class="fa fa-circle-o"></i> Khách sạn</a></li>
          <li><a href="list_customer.php"><i class="fa fa-circle-o"></i> Khách hàng</a></li>
        </ul>
      </li>
    </ul>
  </section>
  <!-- /.sidebar -->
</aside>