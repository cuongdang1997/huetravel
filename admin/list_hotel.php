<?php
	require_once "common/checklogin.php";
	require_once "common/connect.php";
	require_once "../common/sysenv.php";

  $result = $conn -> query($getSQL["getAllHotel"]);
	//insert menu cha
	if($_SERVER["REQUEST_METHOD"] == "POST"){
		if(isset($_POST["save"])){
			//
			$messFalse = "";
			//
			$name = $_POST["name"];
			$status = 1;
			//
			if($name == ""){
				$messFalse = "Vui lòng nhập đầy đủ thông tin";
			}
			//
			if($messFalse == ""){
				$stmt = $conn -> prepare($getSQL["iMenuHotel"]);
				$stmt -> bind_param("si", $name, $status);
				if($stmt -> execute()){
					// $messSuccess = "Lưu thành công";
					header("Location: list_hotel.php");exit();
				} else {
					$messFalse = "Lưu thất bại";
				}
				$stmt -> close();
			}
		}
	}
?>
<!DOCTYPE html>
<html>
<head>
  <meta charset="utf-8">
  <meta http-equiv="X-UA-Compatible" content="IE=edge">
  <title>Administrator | Dashboard</title>
  <!-- Tell the browser to be responsive to screen width -->
  <meta content="width=device-width, initial-scale=1, maximum-scale=1, user-scalable=no" name="viewport">
  <!-- Bootstrap 3.3.7 -->
  <link rel="stylesheet" href="bower_components/bootstrap/dist/css/bootstrap.min.css">
  <!-- Font Awesome -->
  <link rel="stylesheet" href="bower_components/font-awesome/css/font-awesome.min.css">
  <!-- Ionicons -->
  <link rel="stylesheet" href="bower_components/Ionicons/css/ionicons.min.css">
  <!-- Theme style -->
  <link rel="stylesheet" href="dist/css/AdminLTE.min.css">
  <!-- AdminLTE Skins. Choose a skin from the css/skins
   folder instead of downloading all of them to reduce the load. -->
   <link rel="stylesheet" href="dist/css/skins/_all-skins.min.css">
   <!-- Morris chart -->
   <link rel="stylesheet" href="bower_components/morris.js/morris.css">
   <!-- jvectormap -->
   <link rel="stylesheet" href="bower_components/jvectormap/jquery-jvectormap.css">
   <!-- Date Picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-datepicker/dist/css/bootstrap-datepicker.min.css">
   <!-- Daterange picker -->
   <link rel="stylesheet" href="bower_components/bootstrap-daterangepicker/daterangepicker.css">
   <!-- bootstrap wysihtml5 - text editor -->
   <link rel="stylesheet" href="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.min.css">
   <!-- Google Font -->
   <link rel="stylesheet" href="https://fonts.googleapis.com/css?family=Source+Sans+Pro:300,400,600,700,300italic,400italic,600italic">
 </head> 
 <body class="hold-transition skin-blue sidebar-mini">
  <div class="wrapper">

    <!-- include header -->
    <?php include "includes/header.php"; ?>
    <!-- /.include header -->
    
    <!-- include sidebar -->
    <?php include "includes/sidebar.php"; ?>
    <!-- /.include sidebar -->

    <!-- Content Wrapper. Contains page content -->
    <div class="content-wrapper">
      <!-- Content Header (Page header) -->
      <section class="content-header">
      	<div class="container-fluid">
      		<!-- Hotel -->
      		<div class="panel-group">
      			<div class="panel panel-info">
      				<div class="panel-heading text-center"><h4><strong>Khách sạn</strong></h4></div>
      				<div class="panel-body">
      					<?php if(isset($messFalseSubMenuHotel)){ echo "<h5 class='text-center text-danger'>".$messFalseSubMenuHotel."</h5>"; } ?>
      					<?php if(isset($messSuccessSubMenuHotel)){ echo "<h5 class='text-center text-success'>".$messSuccessSubMenuHotel."</h5>"; } ?>
      					<form method="POST">
      						<div class="row">
      							<div class="col-md-4">
      								<div class="form-group">
                        <label>Tên khách sạn</label>
                        <input type="text" name="name" class="form-control" required>
                      </div>
                    </div>
                  </div>
                  <div class="row">
                   <div class="col-md-4">
                    <div class="form-group">
                      <label>Số điện thoại</label>
                      <input type="text" name="phone" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-md-4">
                    <div class="form-group">
                      <label>Địa chỉ</label>
                      <input type="text" name="address" class="form-control" required>
                    </div>
                  </div>
                  <div class="col-md-4 text-center">
                    <div class="form-group">
                      <label></label>
                      <button class="btn btn-success" name="saveSubMenuHotel" value="saveSubMenuHotel"><i class="fa fa-save"></i> Lưu</button>
                    </div>
                  </div>
                </div>
              </form>
            </div>
          </div>
        </div>
      </div>
    </section>
    <!-- Main content -->
    <section class="content-header">
      <div class="container-fluid">
        <!-- Customer -->
        <div class="panel-group">
          <div class="panel panel-info">
            <div class="panel-heading text-center"><h4><strong>Danh sách khách sạn</strong></h4></div>
            <div class="panel-body">
              <table class="table table-striped table-bordered">
                <thead>
                  <tr>
                    <th>STT</th>
                    <th>Tên khách sạn</th>
                    <th>SĐT</th>
                    <th>Địa chỉ</th>
                    <th>Thao tác</th>
                  </tr>
                </thead>
                <tbody>
                  <?php 
                    if ($result->num_rows > 0) {
                      while($rs = $result->fetch_assoc()) {                  
                      echo "<tr>";
                      echo "<td>".$rs["id"]."</td>";
                      echo "<td>".$rs["name"]."</td>";
                      echo "<td>".$rs["phone"]. "</td>";
                      echo "<td>".$rs["address"]."</td>";
                      echo "<td><a class='btn btn-primary mr-2' href='edit_hotel.php?id=".$rs["id"]."'>Sửa</a> <a class='btn btn-danger' href='delete_hotel.php?id=".$rs["id"]."'>Xóa</a></td>";
                      echo "</tr>";
                    }
                  }
                  ?>
                </tbody>
              </table> 
            </div> 
          </div>
        </div>
      </div>
    </section>
    <!-- /.content -->
  </div>
    <!-- /.content-wrapper -->
    
    <!-- include footer -->
    <?php include "includes/footer.html"; ?>
    <!-- /.include footer -->
  </div>
  <!-- ./wrapper -->
  <!-- jQuery 3 -->
  <script src="bower_components/jquery/dist/jquery.min.js"></script>
  <!-- jQuery UI 1.11.4 -->
  <script src="bower_components/jquery-ui/jquery-ui.min.js"></script>
  <!-- Resolve conflict in jQuery UI tooltip with Bootstrap tooltip -->
  <script type="text/javascript" src="plugins/ckeditor/ckeditor.js"></script>
  <script>
    $.widget.bridge('uibutton', $.ui.button);
  </script>
  <!-- Bootstrap 3.3.7 -->
  <script src="bower_components/bootstrap/dist/js/bootstrap.min.js"></script>
  <!-- Morris.js charts -->
  <script src="bower_components/raphael/raphael.min.js"></script>
  <!-- <script src="bower_components/morris.js/morris.min.js"></script> -->
  <!-- Sparkline -->
  <script src="bower_components/jquery-sparkline/dist/jquery.sparkline.min.js"></script>
  <!-- jvectormap -->
  <script src="plugins/jvectormap/jquery-jvectormap-1.2.2.min.js"></script>
  <script src="plugins/jvectormap/jquery-jvectormap-world-mill-en.js"></script>
  <!-- jQuery Knob Chart -->
  <script src="bower_components/jquery-knob/dist/jquery.knob.min.js"></script>
  <!-- daterangepicker -->
  <script src="bower_components/moment/min/moment.min.js"></script>
  <script src="bower_components/bootstrap-daterangepicker/daterangepicker.js"></script>
  <!-- datepicker -->
  <script src="bower_components/bootstrap-datepicker/dist/js/bootstrap-datepicker.min.js"></script>
  <!-- Bootstrap WYSIHTML5 -->
  <script src="plugins/bootstrap-wysihtml5/bootstrap3-wysihtml5.all.min.js"></script>
  <!-- Slimscroll -->
  <script src="bower_components/jquery-slimscroll/jquery.slimscroll.min.js"></script>
  <!-- FastClick -->
  <script src="bower_components/fastclick/lib/fastclick.js"></script>
  <!-- AdminLTE App -->
  <script src="dist/js/adminlte.min.js"></script>
  <!-- AdminLTE dashboard demo (This is only for demo purposes) -->
  <!-- <script src="dist/js/pages/dashboard.js"></script> -->
  <!-- AdminLTE for demo purposes -->
  <script src="dist/js/demo.js"></script>
   <script>
      CKEDITOR.replace('description');
    </script>
</body>
</html>