<?php 
if( $_SERVER["REQUEST_METHOD"] == "POST"){
      //Tạo thư mục lưu file
    $error = array();
    $target_dir = "uploads/";
    //tạo đường dẫn file sau khi upload lên hệ thống    
    $target_file = $target_dir.basename($_FILES['fileToUpload']['name']);

    //Kiểm tra kiểu file hợp lệ
    $type_file = pathinfo($_FILES['fileToUpload']['name'], PATHINFO_EXTENSION);

    $type_file_allow = array('png', 'jpg', 'jpeg', 'gif');
    if(!in_array(strtolower($type_file), $type_file_allow)){
        $error['fileToUpload'] = "File bạn vừa chọn hệ thống không hỗ trợ, bạn vui lòng chọn hình ảnh khác";
    }
    //Kiểm tra kích thước file
    $size_file = $_FILES['fileToUpload']['size'];
    if ($size_file > 1000000) {
        $error['fileToUpload'] = "File của bạn chọn không được quá 1000000";
    }
    // Kiểm tra đã tồn tại trong hệ thống
    if(file_exists($target_file)){
        $error['fileToUpload'] = "File bạn chọn đã tồn tại trên hệ thống";
    }
    //
    if(empty($error)){
        if(move_uploaded_file($_FILES["fileToUpload"]["tmp_name"], $target_file)){
            echo "Bạn đã upload file thành công";
            $flag = true;
        } else {
            echo "File bạn vừa upload gặp sự cố!";
        }
    }
}
?>
