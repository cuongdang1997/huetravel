<?php 
		require_once "common/connect.php";
?>
<!DOCTYPE html>
	<html>
	<head>
		<title>Hue Traveling</title>
		<meta charset="utf-8">
	  	<meta name="viewport" content="width=device-width, initial-scale=1">
		<link rel="stylesheet" href="css/bootstrap-reboot.min.css">
		<link rel="stylesheet" type="text/css" href="css/bootstrap.min.css">
		<link rel="stylesheet" type="text/css" href="css/style.css">
		<meta name="viewport" content="width: device-width, initial-scale =1">
		<link rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.min.css">
		<style type="text/css"></style>
	</head>
	<body>
	<div id="wrapper">
		<div class="header">
			<!-- strat-contact -->
				<div class="row hidden-sm hidden-xs" id="heading">					
					<div class="container">
						<div class="col-md-6 info-contact">
							<i class="fa fa-phone"></i><span> 024 7109 9999</span>
							<i class="fa fa-envelope"></i><span> dulichhue@gmail.com</span>
						</div>
					</div>					
				</div>
			<!-- end-contact -->
			<!-- start menu -->
			<nav class="navbar navbar-expand-lg navbar-light main-menu">
				<div class="container">
				<a class="navbar-brand" href="index.php"><img src="image/logo.png" height="45px" width="45px"></a>
				<button class="navbar-toggler" type="button" data-toggle="collapse" data-target="#navbarSupportedContent" aria-controls="navbarSupportedContent" aria-expanded="false" aria-label="Toggle navigation">
					<span class="navbar-toggler-icon"></span>
				</button>
				
				<div class="collapse navbar-collapse" id="navbarSupportedContent">
					<ul class="navbar-nav ml-auto">
						<li class="nav-item">
							<a class="nav-link" href="index.php" title="Trang chủ">Trang chủ</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#addressTour" title="Địa điểm du lịch">Địa điểm du lịch</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#foodTour" title="Ẩm thực Huế">Ẩm thực Huế</a>
						</li>
						<li class="nav-item">
							<a class="nav-link" href="#tour.php" title="Tour du lịch">Tour</a>
						</li>
						<li class="nav-item">
							<a class="nav-link btn btn-primary text-white text-uppercase ml3" href="#">Đăng nhập</a>
						</li>
						<li>

							
						</li>

					<!-- <li><a href="#" title="Tour">Tour</a></li>
						<li><a href="#" title="Đăng nhập">Đăng nhập</a></li> -->
					</ul>
				</div>
			</div>
	</div>
</div>
</nav>
			<!-- end menu -->
			<!-- start banner -->
			<div class="bg-banner">
				<div class="container wp-banner">
					<div class="row">
						<div class="col-sm-12 col-md-12 col-lg-12 hidden-sm">
							<div id="banner" class="container w-100 h-50 p-0">
								<div class="bd-example">
									<div id="carouselExampleCaptions" class="carousel slide" data-ride="carousel">
										<ol class="carousel-indicators">
											<li data-target="#carouselExampleCaptions" data-slide-to="0" class="active"></li>
											<li data-target="#carouselExampleCaptions" data-slide-to="1"></li>
											<li data-target="#carouselExampleCaptions" data-slide-to="2"></li>
										</ol>
										<div class="carousel-inner">
											<div class="carousel-item active">
												<center><img src="image/banner4.jpg" class="d-block banner-img img-fruit" alt="..."></center>
												
											</div>
											<div class="carousel-item">
												<center><img src="image/banner3.jpg" class="d-block banner-img" alt="..."></center>
												
											</div>
											<div class="carousel-item">
												<center><img src="image/banner2.jpg" class="d-block banner-img" alt="..."></center>
												
											</div>
										</div>
										<a class="carousel-control-prev" href="#carouselExampleCaptions" role="button" data-slide="prev">
											<span class="carousel-control-prev-icon" aria-hidden="true"></span>
											<span class="sr-only">Previous</span>
										</a>
										<a class="carousel-control-next" href="#carouselExampleCaptions" role="button" data-slide="next">
											<span class="carousel-control-next-icon" aria-hidden="true"></span>
											<span class="sr-only">Next</span>
										</a>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div class="wrapper container">
		
		<h3><div id="travelTour"><strong>Tour du lịch</strong><hr></h3>
				<div id="travel"> 
					<div class="row place-wrap-main" class="text-center">
						<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM news ORDER BY id LIMIT 8")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
						?>
							<div class="col-sm-12 col-md-3 col-lg-3 mb-3">
								<a href="traveltour.php?id=<?php echo $id; ?>" class="d-block text-center">
									<p class="text-center mb-2"><?php echo $title ?></p>
									<img src="admin/<?php echo $files; ?>" class="img-fluid frame">
								</a>
							</div>
						<?php
								}
								$stmt -> close();
							}
						?>
				</div>
			</div>
		</div>
	
				<br>
				<br>
				<div class="wrapper container">
				<h3><div id="experience"><strong>Tour trải nghiệm</strong><hr></div></h3>
				<div id="food">
					<div class="row food-wrap-main" class="text-center">
						<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM products ORDER BY id LIMIT 8")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
						?>
							<div class="col-sm-12 col-md-3 col-lg-3 mb-3">
								<a href="traveltour.php?id=<?php echo $id; ?>" class="d-block text-center">
									<p class="text-center mb-2"><?php echo $title ?></p>
									<img src="admin/<?php echo $files; ?>" class="img-fluid frame">
								</a>
							</div>
						<?php
								}
								$stmt -> close();
							}
						?>
					</div>
				</div>
			</div>
			
			<?php include "template/footer.php"; ?>