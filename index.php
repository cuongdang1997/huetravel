	<?php 
		require_once "common/connect.php";
		include "template/header.php";
	?>
	<!-- start introduce -->	
	<div class="wrapper container">
		<h3><div id="main"><strong class="text-center">Giới thiệu về Huế</strong><hr></h3>
			<div id="introduction">
				<div class="row">
					<p class="col-sm-12 col-md-12">Huế là đô thị cấp quốc gia của Việt Nam và từng là kinh đô của Việt Nam thời phong kiến dưới triều nhà Nguyễn (1802 - 1945). Với một bề dày lịch sử khá đồ sộ, Huế đã đem lại cho mình những nét đẹp riêng mà không một nơi nào có được.</p>		
					<div class="col-md-6">
						<div class="text-center"><img src="image/truongTien.jpg" class="img-fluid img-main-about mb-2"></div>
					</div>
					<div class="col-md-6">
						<p> Nằm ở dải đất hẹp của miền Trung Việt Nam, Thừa Thiên Huế là một trong ba vùng du lịch lớn của Việt Nam, là thành phố có bề dày văn hóa lâu đời, cảnh quan thiên nhiên đẹp và hữu tình cùng quần thể di tích lịch sử được thế giới công nhận. </p>
						<p>Ngày nay, Huế được biết đến là thành phố Festival của Việt Nam, lần đầu tổ chức vào năm 2000 và hai năm tổ chức một lần. 
							Huế ngoài nổi tiếng với núi Ngự hùng vĩ soi bóng bên dòng sông Hương thơ mộng nên thơ và các di tích cổ xưa của các triều đại vua chúa thì Huế được biết đến với nhiều bãi biển đẹp cho những ai thích du lịch. 
						Huế còn là địa điểm du lịch lý tưởng không thể bỏ qua đối với những ai yêu thích tìm hiểu, khám phá những di tích lịch sử, văn hóa của Việt Nam.</p>
					</div>
				</div>
				<div class="row">
					<div class="col-md-6">
						<p> Ngoài những cảnh đẹp nổi tiếng thì Huế còn được biết đến với những lễ hội dân gian. Đã thu hút không ít khách du lịch từ nhiều nơi trên đất nước cũng như nước ngoài. </p>
						<p> Với di sản văn hoá thế giới, cảnh quan thiên nhiên, nhiều di tích lịch sử, các sản phẩm đặc sản, nhất là nhà vườn là một nét độc đáo tiêu biểu của thành phố Huế như: nhà vườn An Hiên, Lạc Tịnh Viên, nhà vườn Ngọc Sơn Công Chúa, Tỳ Bà Trang, Tịnh Gia Viên… cùng với hệ thống khách sạn, nhà hàng, các dịch vụ phục vụ khác, thành phố đã và đang trở thành một trung tâm du lịch rất hấp dẫn khách du lịch đến Huế.</p>
					</div>
					<div class="col-md-6">
						<div class="text-center"><img src="image/duaThuyen.jpg" class="img-fluid img-main-about"></div>
					</div>
				</div>
			</div>
			<!--end introduce -->
			<br>
			<h3><div id="addressTour"><strong>Địa điểm du lịch</strong><hr></h3>
				<div id="food"> 
					<div class="row place-wrap-main" class="text-center">
						<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM news ORDER BY id LIMIT 16")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
						?>
							<div class="col-sm-12 col-md-3 col-lg-3 mb-3">
								<a href="placetour.php?id=<?php echo $id; ?>" class="d-block text-center">
									<p class="text-center mb-2"><?php echo $title ?></p>
									<img src="admin/<?php echo $files; ?>" class="img-fluid frame">
								</a>
							</div>
						<?php
								}
								$stmt -> close();
							}
						?>
				</div>
			</div>
	
				<br>
				<br>
				<h3><div id="foodTour"><strong>Ẩm thực Huế</strong><hr></div></h3>
				<div id="food">
					<div class="row food-wrap-main" class="text-center">
						<?php
							if($stmt = $conn -> prepare("SELECT id, title, files FROM products ORDER BY id LIMIT 8")){
								$stmt -> execute();
								$stmt -> bind_result($id, $title, $files);
								while($stmt -> fetch()){
						?>
							<div class="col-sm-12 col-md-3 col-lg-3 mb-3">
								<a href="foodTour.php?id=<?php echo $id; ?>" class="d-block text-center">
									<p class="text-center mb-2"><?php echo $title ?></p>
									<img src="admin/<?php echo $files; ?>" class="img-fluid frame">
								</a>
							</div>
						<?php
								}
								$stmt -> close();
							}
						?>
					</div>
				</div>
			</div>
		<br>
		<?php include "template/footer.php"; ?>
